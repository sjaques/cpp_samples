// Hello padowan!
#include <iostream>

int main()
{
  const std::string myFirstName = "FIRST NAME";
  const std::string myFamilyName = "FAMILY_NAME";
  std::cout << "Hello padowan " << myFirstName << " " << myFamilyName << std::endl;
}

