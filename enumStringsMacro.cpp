#include <iostream>

#define SOME_ENUM(DO) \
    DO(Red) \
    DO(Blue) \
    DO(Yellow) \
    DO(Green) \
    DO(Magenta) \
    DO(Cyan)

#define MAKE_ENUM(VAR) VAR,
enum Colours {
    SOME_ENUM(MAKE_ENUM)
    ColoursCount
};

#define MAKE_STRINGS(VAR) #VAR,
const char* const colours_str[] = {
    SOME_ENUM(MAKE_STRINGS)
    0
};


std::ostream& operator<<(std::ostream& os, enum Colours c)
{
    if (c >= ColoursCount || c < 0) return os << "???";
    return os << colours_str[c];
}

int main()
{
    std::cout << Red << Blue << Green << Cyan << Yellow << Magenta << std::endl;
}
