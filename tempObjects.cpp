#include <iostream>

using namespace std;

struct Obj {
  int i;
  Obj(int _i) : i(_i) {}
};

bool testTempObj(Obj* o) {
  return o != 0;
}

int main() {
  cout << testTempObj(&Obj(5)) << endl;
}

