#include <stdio.h>
#include <stdbool.h>

struct A {
  int i;
  int j;
};


int main() {
  struct A a = {1,2};
  struct A b;
  b = a;

  bool aExists = &a;

  printf("(%d,%d)\n", b.i, b.j);
  return 0;
}

