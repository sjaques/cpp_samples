#include <string>
#include <iostream>
#include <limits.h>
#include <stdlib.h>
#include <sstream>

// See http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c

#ifdef __cplusplus
extern "C"
{
#endif
#define __FILENAME__ (__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__)
#ifdef __cplusplus
}
#endif

std::string currentFileAtRuntime() {
    char full_path[PATH_MAX];
    realpath("foo.dat", full_path);
    return full_path;
}


std::string parentDir(const std::string& filename) {
    std::string s(filename);
    size_t len = s.length();
    char delimiter = '/';

    size_t pos = s.rfind(delimiter); // lastDelimiterPosition
    s.erase(pos, len-pos);
    pos = s.rfind(delimiter); // secondlastDelimiterPosition
    return s.substr(pos+1, len-pos);
}


std::string printParentDirs(const std::string& filename) {
    std::string s(filename);
    std::string delimiter = "/";

    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        std::cout << token << std::endl;
        s.erase(0, pos + delimiter.length());
    }
    std::cout << s << std::endl;
    return s;
}


int main() {

    std::string s(__FILE__);

    std::cout << s << std::endl;
    std::cout << parentDir(s) << std::endl;
    //printParentDirs(s);
}
