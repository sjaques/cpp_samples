#if ( !defined(DAYS_H) || defined(GENERATE_ENUM_STRINGS) )

#if (!defined(GENERATE_ENUM_STRINGS))
    #define DAYS_H
#endif

#include "EnumToString.h"

///////////////////////////////
// The enum declaration
///////////////////////////////
BEGIN_ENUM(Days)
{
    DECL_ENUM_ELEMENT(sunday),
    DECL_ENUM_ELEMENT(monday),
    DECL_ENUM_ELEMENT(tuesday),
    DECL_ENUM_ELEMENT(wednesday),
    DECL_ENUM_ELEMENT(thursday),
    DECL_ENUM_ELEMENT(friday),
    DECL_ENUM_ELEMENT(saturday)
}
END_ENUM(Days)

#endif // (!defined(DAYS_H) || defined(GENERATE_ENUM_STRINGS))

#include <stdio.h>

int main()
{
    tagDays day = thursday;
    switch( day )
    {
        case monday:
        case tuesday:
        case wednesday:
        case thursday:
        case friday:
        {
            printf("Today is %s, I have to work!", GetStringDays(day) );
        }
        break;

        case saturday:
        case sunday:
        {
            printf("Today is %s, very nice!!!", GetStringDays(day) );
        }
        break;
    }
    return 0;
}


