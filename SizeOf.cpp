#include <iostream>

int main()
{
    std::cout << "Size of long[4]: " << sizeof(unsigned long[4]) << '\n';
    std::cout << "Size of char: " << sizeof(char) << '\n';
    std::cout << "Size of short: " << sizeof(short) << '\n';
}
