#include <iostream>

struct Struct {
	int i;
};

int main()
{
	Struct s = {};
	Struct* p = &s;
	Struct** pp = &p;

	std::cout << "&((*pp)->i) : " << &((*pp)->i) << std::endl;
	std::cout << "&(*pp)->i   : " << &(*pp)->i << " -> takes still reference of 'i'" << std::endl;
}
