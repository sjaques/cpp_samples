
#include <string>
#include <string.h>
#include <iostream>

//some C struct definition

#define HG_TOOLS_VERSION_SIZE (6+41)
#define BUILD_SW_VERSION_PREFIX_SIZE (11)
#define BUILD_SW_VERSION_SIZE (BUILD_SW_VERSION_PREFIX_SIZE+11)

#define XSTR(s) #s

#define ___BLDINFO__ __attribute__ ((section(".bldinfo")))

struct build_info_s {
    char tools_version [HG_TOOLS_VERSION_SIZE];
    char sw_version [BUILD_SW_VERSION_SIZE];
    char end[12]; /* always leave this last */
};

struct build_info_s build_info ___BLDINFO__ =
{
  "tools=" XSTR(HG_TOOLS_VERSION),
  "sw_version=" XSTR(SW_VERSION),
  "BLDINFO END"
};

int main() {
    std::string swVersionStr(build_info.sw_version, sizeof(build_info.sw_version));
    swVersionStr = swVersionStr.substr(BUILD_SW_VERSION_PREFIX_SIZE);
    std::cout << swVersionStr << std::endl;
}
