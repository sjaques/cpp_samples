#include <iostream>

void ZynqUpgrStream(char const* in, unsigned cpu)
{
    std::cout << "ZynqUpgr: cpu[" << cpu << "] " << in;
}

int main()
{
    ZynqUpgrStream("Test", 1);
}
