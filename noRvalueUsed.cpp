#include <iostream>
#include <string>

struct S {
    std::string i;
};

void function(S&& s)
{
   std::cout << __PRETTY_FUNCTION__ << s.i << "!\n";
}

void function(S &s)
{
   std::cout << __PRETTY_FUNCTION__ << s.i << "!\n";
}

int main()
{
  S&& test{"test"};
  function(test);  // prints "void function(S&)test!"
}
