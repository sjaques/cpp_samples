#include <iostream>


void print(int array[], unsigned long int size)
{
    for(unsigned long int i = 0; i < size; ++i)
    {
        std::cout << "array[" << i << "]: " << array[i] << std::endl;
    }
}

void print(int& number){ std::cout << "Number: " << number << std::endl;  }

int main()
{

    int array[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    print(array, sizeof(array)/sizeof(array[0]));

    int& element = array[0]; // HERE IS THE BUG!
    for(unsigned long int i = 0; i < sizeof(array)/sizeof(array[0]); ++i)
    {
        element = array[i]; // VALUE IS ASSIGNED INSTEAD OF REFERENCE (YOU CAN'T CHANGE A LVALUE REFERENCE)
        print(element);
    }


    print(array, sizeof(array)/sizeof(array[0]));
    return 0;
}

