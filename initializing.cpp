// Compile using g++ -std=c++11 -g

#include <string>
#include <iostream>

std::string s{'a', 'b', 'c'};
char a[5] = {'a', 'b', 'c'};
std::string s2("hello");
std::string s3;

int main() {
  s3 = "s3";
  std::cout << s3 << std::endl;
}
