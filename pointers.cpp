#include <memory>
#include <iostream>

int main() {
    int *ptr = 0;
    delete ptr;  // no error

    auto uniquePtr = std::make_unique<int>(10);
    uniquePtr.reset();
    uniquePtr.reset();  // still fine, it has been released already

    auto ownedByOther = std::make_unique<int>(11);

    int* deletedByOther = ownedByOther.release();
    std::cout << ownedByOther.get() << std::endl;

    /// other
    delete deletedByOther;

}
