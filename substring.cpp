#include <iostream>
#include <string.h>

const char sw_version[] = "sw_version=R5.7";

unsigned versionOnlyLength = strlen(sw_version) - strlen("sw_version=");

int main()
{

    std::cout << versionOnlyLength << std::endl;

}
