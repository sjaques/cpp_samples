#include "functionInNamespace.h"

using namespace test; // Not linking
// f() needs to be surrounded with:
// namespace test {
void f() {

}


int main() {
    test::f();
}
