#include <algorithm>
#include <iostream>
#include <typeinfo>

class MyClass
{
public:
    // ...
    friend void swap(MyClass & first, MyClass & second)
    {
        using std::swap; // enable ADL
        swap(first.mSize, second.mSize);
        swap(first.mArray, second.mArray);
    }

    MyClass(size_t s = 0) : mSize(s), mArray(mSize ? new int[s] : 0) {}

    MyClass(MyClass const & other) : mSize(other.mSize), mArray(other.mSize ? new int[mSize] : 0)
    {
        std::copy(other.mArray, other.mArray + other.mSize, mArray);
    }

    MyClass& operator=(MyClass other)
    {
        swap(*this, other);
        return *this;
    }

    unsigned mSize;
    int* mArray;
};


int main() {
  MyClass a(5);

  std::cout << "Class "<< quote(a) << " created (size = "<< a.mSize << " )" << std::endl;

  MyClass b(a);

  std::cout << "Class b created (size = "<< b.mSize << " )" << std::endl;

  MyClass c(10);

  std::cout << "Class c created (size = "<< c.mSize << " )" << std::endl;

  b = c;
  std::cout << "b = c succeeded (size = "<< b.mSize << " )" << std::endl;

}
