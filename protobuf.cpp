// -------------------------------------------------
// PlayerProfile_Struct.proto
message PlayerProfile_Struct{
  required uint32 checksum = 1;
  required uint32 gender = 2;
  optional string name = 3;
}

int main() {
	// -------------------------------------------------
	// Create an instance and serialize it to a file.
	PlayerProfile_Struct player;
	player.set_checksum(50193);
	player.set_name("Alex&Anni");
	player.set_gender(2);

	fstream out("alex_anni.pb", ios::out | ios::binary | ios::trunc);
	player.SerializeToOstream(&out);
	out.close();

	// -------------------------------------------------
	// Create an instance and deserialize it from a file.
	PlayerProfile_Struct player;
	fstream in("alex_anni.pb", ios::in | ios::binary);
	if (!player.ParseFromIstream(&in)) {
		cerr << "Failed to parse alex_anni.pb." << endl;
		exit(1);
	}

	if (player.has_name()) {
		cout << "e-mail: " << player.name() << endl;
	}

}
