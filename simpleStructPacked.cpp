#include <stdio.h>

int main() {

    struct str {
        char a;
        int arr;
    };

    struct str_packed {
        char a;
        int arr;
    }  __attribute__((packed));

    int a[4];
    int *ptr = a;
    char x = 212;

    printf("sizeof struct str = %u\n", sizeof(str));
    printf("sizeof struct str_packed = %u\n", sizeof(str_packed));
    printf("sizeof struct a = %u\n", sizeof(a));
    printf("sizeof struct ptr = %u\n", sizeof(ptr));
    printf("Unsigned char x = %x\n", int(x));
    return 0;
}

