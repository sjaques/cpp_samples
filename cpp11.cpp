#include <iostream>

struct Test {
  unsigned int var = 1;
};

int main() {
  Test t;

  std::cout << t.var << std::endl;
}
