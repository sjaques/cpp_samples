#include <utility>

struct strange {
  int count {0};  // fixed by making mutable, duh
  strange(int count) : count(count) {}
//  strange( strange const&& o):count(o.count) { o.count = 0; }
};

int main()
{
const strange s { 1 };
const strange s2 = std::move(s);
}
