#include <iostream>

using namespace std;


static const size_t MAX = 5;

void loopDown0to4() {
    cout << __FUNCTION__ << endl;

    for(size_t i = MAX; i-- > 0; )
    {
        cout << i << endl;
    }
}

void doWhileLoopUp0to4()
{
    cout << __FUNCTION__ << endl;

    size_t i = 0;
    do {
        cout << i << endl;
    } while (++i < MAX);
}
void whileLoopUp1to5() {
    cout << __FUNCTION__ << endl;

    size_t i = 0;
    while( i++ < MAX) {
        cout << i << endl;
    }
}


int main () {

    loopDown0to4();
    doWhileLoopUp0to4();
    whileLoopUp1to5();
}
