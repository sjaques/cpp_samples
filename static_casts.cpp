// Example program
#include <iostream>
#include <string>

int main()
{
  std::uint64_t bigUnsigned = 0xFFFFFFFF00000000;
  std::uint32_t smallUnsigned = 0xFFFF0000;
  std::uint16_t tinyUnsigned = 0xFF00;

  std::int64_t bigSigned = 0x00000000FFFFFFFF;
  std::int32_t smallSigned = 0x0000FFFF;
  std::int16_t tinySigned = 0x00FF;

  std::cout << "INIT: bigUnsigned=" << std::hex << bigUnsigned << "\n";
  std::cout << "INIT: smallUnsigned=" << std::hex << smallUnsigned << "\n";
  std::cout << "INIT: bigSigned=" << std::hex << bigSigned << "\n";
  std::cout << "INIT: smallSigned=" << std::hex << smallSigned << "\n";

  //// Generates same warning!: warning: conversion to ‘uint64_t {aka long unsigned int}’ from ‘int64_t {aka long int}’ may change the sign of the result [-Wsign-conversion]
  std::cout << "bigUnsigned=" << std::hex << (bigUnsigned = bigSigned) << "\n";
  std::cout << "bigUnsigned=" << std::hex << static_cast<decltype(bigUnsigned)>(bigUnsigned = bigSigned) << "\n";

  //// Generates same warning!: warning: conversion to ‘uint64_t {aka long unsigned int}’ from ‘int64_t {aka long int}’ may change the sign of the result [-Wsign-conversion]
  std::cout << "bigUnsigned=" << std::hex << (bigUnsigned = bigSigned) << "\n";
  std::cout << "bigUnsigned=" << std::hex << static_cast<decltype(bigUnsigned)>(bigUnsigned = bigSigned) << "\n";

  std::cout << "smallUnsigned=" << std::hex << smallUnsigned << "\n";
  std::cout << "bigSigned=" << std::hex << bigSigned << "\n";
  std::cout << "smallSigned=" << std::hex << smallSigned << "\n";

}

