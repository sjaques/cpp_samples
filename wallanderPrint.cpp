#include <iostream>
#include <stdlib.h>
#include <stdarg.h>
#include <syslog.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

bool use_syslog = false;

int wallander_printf(int level, const char *format, ...) {

    int chars_printed = 0;

    va_list args;
    va_start(args, format);

    if (use_syslog) {
      // prepend slotnumber
      if (format) {
        char* formatstring = new char;
        sprintf(formatstring, "[%d] %s", 1, format);
        vsyslog(LOG_DEBUG, formatstring, args);
        delete formatstring;
        chars_printed = 1;
      }
    }

    chars_printed = vprintf(format, args);
    fflush(stdout);
    va_end(args);

    return chars_printed;
}


int main()
{
    wallander_printf(1, "This is a %s %s", "test", "/r/n");
    return 0;
}
