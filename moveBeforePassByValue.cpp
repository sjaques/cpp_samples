// Pass struct as rvalue because of move only member
#include <iostream>
#include <string>
#include <memory>

struct logger {
    logger(std::string s ) : str(s) { std::cout << __FUNCTION__ << '\n'; }
    ~logger() { std::cout << __FUNCTION__ << '\n'; }
    explicit operator std::string() { return str; }
    std::string str;
};

struct S {
    std::unique_ptr<logger> i;
};

// Pass by value (rvalue in this only case)
std::string f(S s) {
    return s.i->str;
}

int main()
{
  S s{std::make_unique<logger>("sam")};
  
  std::cout << "Hello " << f(std::move(s)) << "!\n";
}