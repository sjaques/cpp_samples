#include <cstdlib>
#include <iostream>

class Base
{
    private:
        virtual void print(std::ostream& str) const = 0;
    public:
        friend std::ostream& operator<<(std::ostream& str, Base const& data)
        {
            data.print(str);
            return str;
        }
};

class Derived : public Base
{
    public:
        virtual void print(std::ostream& str) const {
           str << "Derived::print";
        }
};

int main()
{
    Derived d;
    Base& b = d;
    std::cout << b << std::endl;
    return EXIT_SUCCESS;
}

