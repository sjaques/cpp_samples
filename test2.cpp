#include <stdio.h>
#include <string.h>

int main() {
    const unsigned boardNameSize_c = 8;
    const unsigned selfTestSize_c = 24;
 
    struct BaseIndHeaderLsm1
    {
      unsigned char  messageId_m;
      unsigned char  swVersionTypeBLength_m;   // old R4.x semantics: passiveVersionLength_m
    } base1;

    struct BaseIndHeaderLsm2 : public BaseIndHeaderLsm1
    {
      unsigned char  messageId_m;
      unsigned char  msgVersionId_m;
      unsigned char  FIAcodeMSB_m;             // from RIP
      unsigned char  FIAcodeLSB_m;
      unsigned char  name_m[boardNameSize_c];  // mnemonic of the board, from RIP
      unsigned char  selftestUsed_m;           // info flags
      unsigned char  selftest_m[selfTestSize_c];
      unsigned char  phyRackId_m;
      unsigned char  phyShelfId_m;
      unsigned char  phySlotId_m;
      unsigned char  startUpState_m;          
      unsigned char  swVersionTypeALength_m;   // old R4.x semantics: activeVersionLength_m
      unsigned char  swVersionTypeBLength_m;   // old R4.x semantics: passiveVersionLength_m
    } __attribute__ ((packed)) base2;

    struct BaseIndHeaderLsm3 : public BaseIndHeaderLsm1
    {
      unsigned char  messageId_m;
      unsigned char  msgVersionId_m;
      unsigned char  FIAcodeMSB_m;             // from RIP
      unsigned char  FIAcodeLSB_m;
      unsigned char  name_m[boardNameSize_c];  // mnemonic of the board, from RIP
      unsigned char  selftestUsed_m;           // info flags
      unsigned char  selftest_m[selfTestSize_c];
      unsigned char  phyRackId_m;
      unsigned char  phyShelfId_m;
      unsigned char  phySlotId_m;
      unsigned char  startUpState_m;          
      unsigned char  swVersionTypeALength_m;   // old R4.x semantics: activeVersionLength_m
      unsigned char  swVersionTypeBLength_m;   // old R4.x semantics: passiveVersionLength_m
    } base3;

    printf("Base size = %u\n", sizeof(base1));
    printf("Base2 size = %u\n", sizeof(base2));
    printf("Base3 size = %u\n", sizeof(base3));

    char mBootVersionReduce[7];
    printf("BootVersion length = %u\n", strlen(mBootVersionReduce));
    struct str {
        char a;
        int arr[2];
    }  __attribute__((packed)) test;

    struct str2 {
	char a;
	int arr[2];
    } test2;

    printf("sizeof struct = %u\n", sizeof(test));
    printf("sizeof struct = %u\n", sizeof(test2));
    return 0;
}

