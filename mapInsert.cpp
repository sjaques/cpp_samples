#include <map>
#include <iostream>

struct Client {
    Client() { std::cout << "Construction!" << std::endl; }
    Client(const Client&) { std::cout << "Copy made!" << std::endl; }
};

int main() {
    std::map<int, Client> myMap;

    std::cout << "[1] operator[]" << std::endl;
    myMap[0] = Client();

    std::cout << "[2] insert() without const key" << std::endl;
    myMap.insert(std::pair<int, Client>(1,Client()));

    std::cout << "[3] insert() with const key" << std::endl;
    myMap.insert(std::pair<int const, Client>(2,Client()));

}
