#include <iostream>
#include <string>

struct S
{
    S() = default;
    S(const S &o) { std::cerr << __PRETTY_FUNCTION__ << std::endl; }
    S(S &&o) { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    long _i{444};
};

long only_allow_rvalues(S &&s)  // <-- clue of this file is, forbids to pass an lvalue
{
    std::cout << __FUNCTION__ << std::endl;
    S other = std::move(s);
    return other._i;
}

long allow_r_and_lvalues(S s)  // semantically identical as only_allow_rvalues
{
    std::cout << __FUNCTION__ << std::endl;
    S other = std::move(s);
    return other._i;
}

int main()
{
    std::cout << "---" << std::endl;
    only_allow_rvalues(S{});

    std::cout << "---" << std::endl;
    S lvalue;
    allow_r_and_lvalues(lvalue); // copied upon calling

    std::cout << "---" << std::endl;
    S almost_rvalue;
    allow_r_and_lvalues(std::move(almost_rvalue));
}