#include <stdio.h>
#include <stdint.h> 

int main()
{
struct S0 {
    // will usually occupy 2 bytes:
    // 3 bits: value of b1
    // 5 bits: unused
    // 6 bits: value of b2
    // 2 bits: value of b3
    uint8_t b1 : 3;
    unsigned char :0; // start a new byte
    unsigned char b2 : 6;
    unsigned char b3 : 2;
} tmp0;
class S1 {
    // will usually occupy 2 bytes:
    // 3 bits: value of b1
    // 5 bits: unused
    // 6 bits: value of b2
    // 2 bits: value of b3
    unsigned b1 : 3;
    unsigned char :0; // start a new byte
    unsigned char b2 : 6;
    unsigned char b3 : 2;
} __attribute__ ((packed)) tmp1;

printf("Size of tmp0 = %u\n", sizeof(tmp0));
printf("Size of tmp1 = %u\n", sizeof(tmp1));

return 0;
}
