#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    char word[10] = "1234";

    char* h_word = malloc(sizeof(word));
    memcpy(h_word, &word, sizeof(word));

    printf("Word size: %d\n", sizeof(word));
    printf("Word length: %d\n", strlen(word));
    printf("Heap word size (only byte size of ptr): %d\n", sizeof(h_word));

    h_word = realloc(h_word, strlen(word));
    printf("Heap word length after realloc: %d\n", strlen(h_word));
    printf("Except the heap word length to be the same as the word length.\n");

    free(h_word);
    return 0;
}
