#include <iostream>

#define COMMAND_SUCCESS  (0x8000)
#define COMMAND_FAIL     (0x4000)
#define PROGRAM_PKG         (0x2)

unsigned long master_cmd_reg = PROGRAM_PKG;
unsigned char currentRegValue = COMMAND_FAIL | master_cmd_reg;

unsigned long returnedStatus = (currentRegValue & master_cmd_reg);

int main () {

std::cout << "currentRegValue" << int(currentRegValue) << std::endl;
std::cout << "returnedStatus" << returnedStatus << std::endl;

return 0;

}
