#include <vector>
#include <iostream>
#include <algorithm>

 struct S
 {
     S(int _i) : i(_i) {}
     ~S() { std::cout << "called destructor" << std::endl; }

     int i;
 };

void print_container(const std::vector<S>& c)
{
    for (const S &s : c) {
        std::cout << s.i << " ";
    }
    std::cout << '\n';
}

int main( )
{
    std::vector<S> c{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    print_container(c);

    c.erase(std::remove_if(c.begin(), c.end(), [](const S &s) { return (s.i % 2) == 0; }), c.end());
    print_container(c);
}

