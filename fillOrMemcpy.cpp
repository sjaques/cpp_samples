#include <iostream>
#include <cstring>
#include <sstream>

#include <stdint.h>

//  Windows
#ifdef _WIN32
#include <intrin.h>
uint64_t rdtsc(){
    return __rdtsc();
}
//  Linux/GCC
#else
uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}
#endif

struct MyStruct
{
    int a;
    int b;
};

int main(int argc, char* argv[]) {

    MyStruct abc;
    abc.a = 123;
    abc.b = 321;

    const size_t nbrOfItems = 1000000;
    MyStruct arr[nbrOfItems];

    if (argc != 2) { std::cout << "provide 0 (fill_n) or 1 (memset) \n"; return 1; }

    std::istringstream ss(argv[1]);
    int x;
    if (!(ss >> x)) { std::cout << "provide 0 (fill_n) or 1 (memset) \n"; return 1; }

    unsigned number_of_runs = 1000;
    uint64_t elapsed_clocks = 0;
    for (unsigned i = 0; i < number_of_runs; ++i) {
        uint64_t begin = rdtsc();
        if (x == 0) {
            std::cout << "Using fill_n \n";
            // fill 100 MyStruct's with copy of abc
            std::fill_n(arr, nbrOfItems, abc); // working C++ way
        } else {
            std::cout << "Using memcpy \n";
            std::memset(arr, 0, nbrOfItems);
        }
        uint64_t end = rdtsc();

        elapsed_clocks += end - begin;
    }

    std::cout << "Average clock ticks per run: "<<  elapsed_clocks/number_of_runs << '\n';
    return 0;
}
