# To build yAPI examples from this directory, type for example
#
#   make IVY=ivy BUILDROOT_DEFCONFIG=isam-reborn-x86-qemu_defconfig
#
# You can build a single example like this
#
#   make IVY=ivy BUILDROOT_DEFCONFIG=isam-reborn-x86-qemu_defconfig ylog_example
#
# Note that you must first build yAPI in y/build/yapi directory.

include ../../../../build/common/root/root.mak
TARG_ARCH := REBORN
OPTIMIZE = speed

# Set flags to empty because they will be inherited from the parent
# Makefile in case of recursive make from y/build, which will
# lead to double flags when we include BLDCOMMON further down here
CCFLAGS = -std=c++11
CFLAGS =
LOCALCFLAGS =
GLOBALCFLAGS =

include $(VIEWROOT)/vobs/esam/build/reborn/common/common.mak
include $(VIEWROOT)/vobs/esam/build/reborn/common/isam_app/common.mak
include $(VIEWROOT)/vobs/esam/build/reborn/common/buildroot_rules.mak

LDFLAGS += -L$(VIEWROOT)/y/build/yapi -lyAPI -levent_core -lprotobuf -lpthread -lstdc++

SRC_DIR = $(VIEWROOT)/y/src
BLD_DIR = $(ROOT)/sw
EXAMPLES_DIR = $(SRC_DIR)/supervisor/examples

EXPS_CXX = $(wildcard $(EXAMPLES_DIR)/*.cpp)
EXAMPLES = $(addprefix $(CURDIR)/,$(patsubst %.cpp,%,$(notdir $(EXPS_CXX))))
$(info Build examples: EXAMPLES="$(EXAMPLES)")

DEPS_C = $(SRC_DIR)/supervisor/lib/startstop.c $(SRC_DIR)/ycommon/main/main.c

OBJS_C = $(subst .c,.o,$(DEPS_C))
OBJS_CXX = $(subst .cpp,.o,$(EXPS_CXX))


all: $(EXAMPLES) clean_obj  #todo: get rid of this makefile -> use app way

# allow calling "make <example>" by defining appropriate targets
# note that pattern rules must have at least one command
%: $(CURDIR)/%
	true

%.o: %.c
	cd $(BLD_DIR) && $(CC) -c $(CFLAGS) $< -o $@

%.o: %.cpp
	cd $(BLD_DIR) && $(CCC) -c $(CCFLAGS) $< -o $@

$(EXAMPLES): $(OBJS_C) $(OBJS_CXX) # link
	cd $(BLD_DIR) && $(CC) -o $@ $(OBJS_C) $(OBJS_CXX) $(LDFLAGS)

clean_obj: #todo: get rid of this makefile -> use app way
	rm -f $(OBJS_CXX) $(OBJS_C)

clean: clean_obj #todo: remove dirty clean_obj
	rm -f $(EXAMPLES)

makeclean: clean all

include $(ROOT)/sw/flat/BLDCOMMON/tools.mak
