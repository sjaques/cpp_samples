#include <iostream>

struct Pod {
  int i;
  int j;
};

int main ()
{
  Pod pod1;
  Pod pod2 = Pod();

  std::cout << "pod1.i = " << pod1.i << " pod1.j = " << pod1.j << std::endl;
  std::cout << "pod2.i = " << pod2.i << " pod2.j = " << pod2.j << std::endl;
}
