#include <iostream>

enum Enum {
	a = 1,
	b
};

int main ()
{
	Enum enumVar;
	bool c = true;
	std::cout << "A " << (b == c) << std::endl;
	std::cout << "B " << (static_cast<bool>(b) == c) << std::endl;
	std::cout << "C " << (b == static_cast<Enum>(c)) << std::endl; // <== same as A!
	std::cout << "D " << (static_cast<int>(b) == static_cast<int>(c)) << std::endl; // <== same as A!
}
