#pragma once
#include <functional>
#include "c_callback.h"

int cpp_call_callback(int x, void *ctx);
void cpp_clean_ctx(void *ctx);

using callback_t = std::function<int (int)>;

inline int cpp_api(callback_t cb)
{
    return c_api(cpp_call_callback, new callback_t(cb));
}

