#include <functional>
#include "c_callback_wrapper.h"

void cpp_clean_ctx(void *ctx)
{
    delete static_cast<callback_t *>(ctx);
}

int cpp_call_callback(int x, void *ctx)
{
    auto *func = static_cast<callback_t *>(ctx);
    int ret = (*func)(x);
    delete func;
    return ret;
}
