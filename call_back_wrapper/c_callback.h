#pragma once
#ifdef __cplusplus
extern "C" {
#endif

typedef int (*callback)(int, void *);

int c_api(callback cb, void *ctx);
int call_registered_callback(int x);

#ifdef __cplusplus
}
#endif
