#include <stddef.h>
#include "c_callback.h"

callback callback_store = NULL;
void *ctx_store = NULL;

int c_api(callback cb, void *ctx)
{
    /* do most important stuff */

    callback_store = cb; // save cpp callback func
    ctx_store = ctx; // std::function with c callback func
    return 0;
}

int call_registered_callback(int x)
{
    int ret = -1;
    if (callback_store) {
        ret = callback_store(x, ctx_store);
    }
    callback_store = NULL;
    ctx_store = NULL;
    return ret;
}
