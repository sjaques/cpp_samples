#include <iostream>
#include "c_callback_wrapper.h"

int a = 3;
int b = 4;

int plus(int x) {
    return (a + b) * x;
}
int min(int x) {
    return (a - b) * x;
}
int main(void)
{

    // FIRST TEST
    cpp_api(plus);
    //after some time next call will get called by yapi
    std::cout << call_registered_callback(5) << std::endl;


    // NEXT TEST
    cpp_api(min);
    // after some time next call will get called by yapi
    std::cout << call_registered_callback(10) << std::endl;
}
