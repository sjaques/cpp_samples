#include <functional>
#include <sstream>
#include <type_traits>
#include <vector>
#include <iostream>

class Streamer {
public:

    template<typename T, std::size_t N> // see info
    Streamer &operator<<(T (&)[N])
    {
        static_assert(false);
        return *this;
    }

    template<typename T>
    Streamer &operator<<(T &&value)
    {
        _stream << std::forward<T>(value);
        std::cout << __FUNCTION__ << " | is array: " << std::is_array<decltype(value)>::value << std::endl;

        return *this;
    }

private:
    std::ostringstream _stream;
};

int main () {
    Streamer m;

    constexpr const char lala[] = "lala";
    std::cout << __FUNCTION__ << " | is array: " << std::is_array<decltype(lala)>::value << std::endl;

    m << lala; // same as `m << "lala";` --> C-string is also interpreted as a C-array
}